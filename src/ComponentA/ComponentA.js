(function () {

  angular.module('sampleLib').directive('componentA', function () {
    return {
      template: require('./ComponentA.html'),
      controller: ComponentAController,
      controllerAs: 'ctrl',
      bindToController: true
    };
  });

  function ComponentAController() {
    this.name = 'Child Component A';
  }
})();
(function () {

  angular.module('sampleLib').directive('componentB', function () {
    return {
      template: require('./ComponentB.html'),
      controller: ComponentBController,
      controllerAs: 'ctrl',
      bindToController: true
    };
  });

  function ComponentBController() {
    this.name = 'Child Component B';
  }
})();
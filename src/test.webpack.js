// This file is an entry point for angular tests
// Avoids some weird issues when using webpack + angular.

import angular from 'angular';
import 'angular-mocks';

const { module, inject } = angular.mock;
window.module = module;
window.inject = inject;
console.log(process.cwd());
const context = require.context(process.cwd(), true, /\.js$/);

context.keys().forEach(context);

